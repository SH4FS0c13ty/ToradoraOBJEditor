![SH4FS0c13ty](https://i.ibb.co/ZfSMNpT/SH4-FS0c13ty.png)

## New TigerXDragon version

I am currently working on a new version of TigerXDragon that will improve the code, tools, automation, ...<br/>
New version information:<br/>
Name and version: TigerXDragon v2.0.0<br/>
Codename: Taiga Aisaka (Toradora! reference)<br/>
Release URL: https://sh4fs0c13ty.tk/apps/TigerXDragon/TigerXDragon_v2.7z (Currently not released, so an error 404 will be thrown)<br/>
I haven't started working on it because I'm already working on Anti-Cheat v2 and my studies take a lot of time.<br/>
Anyway, the toolkit works already (even if it's not as optimized as it should be) but I want to make it better.<br/>
I also want it to be cross-platform, that's why I will switch to Python 3.8 for this project.<br/>
In the next major version, you will not have to do the steps manually as there will be a "menu" to help you.<br/>
I will also improve the tools' code to make them easier to use and reorganize folder (because, let's be honest, it's a mess out there).<br/>
As I said, I haven't started working on it, BUT I have written several ideas, new functionalities or improvements.<br/>
It's not a dead project, it has just been slowed down by lack of time (and motivation, sleeping is way better xD).<br/>
If you wanna help us, feel free to contact me on Discord (SH4FS0c13ty#1562) or go to https://toradora-fr.tk/ .<br/>
Look out for the new version of TigerXDragon!<br/>

# Toradora! Portable OBJ Editor

Toradora! Portable OBJ Editor is a part of the TigerXDragon toolkit (https://gitlab.com/SH4FS0c13ty/TigerXDragon).<br />
This project contains a DLL library file and an Executable file in C#.<br />
This project has been created to translate the strings (OBJ files) in Toradora! Portable game.<br />


## License

MIT License (https://opensource.org/licenses/mit-license.php)<br />

Copyright (c) 2019 SH4FS0c13ty<br />

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and<br />
associated documentation files (the "Software"), to deal in the Software without restriction,<br />
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,<br />
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,<br />
subject to the following conditions:<br />

The above copyright notice and this permission notice shall be included in all copies or substantial<br />
portions of the Software.<br />

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT<br />
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.<br />
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,<br />
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE<br />
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.<br />
<br />

## Credits

Original OBJ Editor took from "Toradora! Portable.rar" by Marcus André:<br />
https://github.com/marcussacana/Specific-Games/tree/master/Stuff's<br />
I modified the program to make it more automatic in order to use it in the TigerXDragon toolkit.
